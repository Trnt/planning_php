<!DOCTYPE html>
<html lang="fr">

  <head>
    <meta charset ="utf-8" />
    <title> Formulaire d'inscription </title>
    <link rel="stylesheet" href="css/inscription.css"/>
  </head>

  <body>
    <header> Planning </header>

    <section>

      <h1>Formulaire d'inscription</h1>
      <p>Veuillez remplir les champs suivants afin de vous créer un compte</p>

      <form action="register.php" method=POST >
        <table>
		<tr>
		<?php
			if (isset($_GET["erreur1"])){
				echo "Vous êtes l'heureux 11eme utilisateur, c'est complet";
			}
		 ?>
		</tr>

        	<tr>
            <td>Nom : </td>
            <td><input type="text" name="nom" id="nom"> </td>
          </tr>

          <tr>
        	   <td>Prénom : </td>
             <td><input type="text" name="prenom" id="prenom"></td>
          </tr>

          <tr>
        	   <td>Pseudo : </td>
             <td><input type="text" name="pseudo" id="pseudo"></td>
          </tr>

          <tr>
        	   <td>Mot de passe : </td>
             <td><input type="password" name="pwd" id="pwd"></td>

          </tr>

          <tr>
            <td>Confirmation du mot de passe : </td>
            <td><input type="password" name="recon" id="conf"></td>
          </tr>

        </table>

        <input type="submit" id="submit"> </br>
    	</form>

    </section>

    <footer> Ce site à été réalisé par Anaïs Trinité et Chaïma Sakhi </footer>
  </body>

</html>
