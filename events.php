<?php
  session_start();
  // liste des événements
   $json = array();
   // requête qui récupère les événements
   $requete = "SELECT * FROM ACTIVITE where idUtil = ".$_SESSION['idUser'];

   // connexion à la base de données
   try {
    $bdd = new PDO('sqlite:bd_planning.SQLite3');
     $bdd -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


     // exécution de la requête
     $resultat = $bdd -> query($requete) or die(print_r($bdd -> errorInfo()));


     // envoi du résultat au success
     echo json_encode($resultat -> fetchAll(PDO::FETCH_ASSOC));

   }

   catch(Exception $e) {
     exit('Impossible de se connecter à la base de données.   '.$e);
   }

?>
