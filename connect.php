<html>
  <head>
    <meta charset ="utf-8" />
    <title> Connection </title>
    <link rel="stylesheet" href="css/inscription.css"/>
  </head>

  <body>
    <header> Planning </header>
    <section>

      <h1 id = 'titre'>Connection</h1>
      <p>Veuillez entrer vos identifiants pour vous connecter</p>

      <form action="verification.php" method=POST >

        <table>
      		<tr>
      		 <?php
      			if (isset($_GET["erreur"])){
      				echo "<p id = 'error'> Votre identifiant ou votre mot de passe est incorect </p>";
      			}
      		 ?>
      		</tr>
          <tr>
            <td>Pseudo : </td>
            <td><input type="text" name="pseudo" id="pseudo"></td>
          </tr>

          <tr>
              	<td>Mot de passe : </td>
              <td><input type="password" name="pwd" id="pwd"  pattern=".{3,}"></td>
          </tr>

      		<tr>
            <td> </td>
            <td class = 'wanna'><input id = 'submit'  type="submit" value="Se connecter"> </br></td>
          </tr>

          <tr  class = 'jsp'>
    			     <td> Vous n'avez pas de compte ? </td>
               <td><a href="inscription.php" > Inscrivez vous ici ! </td>
    			</tr>

        </table>
      </form>
    </section>

    <footer> Ce site à été réalisé par Anaïs Trinité et Chaïma Sakhi </footer>

  </body>

</html>
