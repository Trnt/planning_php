<?php
try{
 $bdd = new PDO('sqlite:bd_planning.SQLite3');
$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 
      $bdd -> exec('CREATE TABLE IF NOT EXISTS USER(
                      idUser INTEGER PRIMARY KEY,
					  nomUser TEXT,
                      prenomUser TEXT,
					  pwd TEXT,
                      pseudo TEXT);

		');


	$bdd -> exec('CREATE TABLE IF NOT EXISTS ACTIVITE(
			id INTEGER PRIMARY KEY,
			start TIME,
			end TIME,
			title TEXT NOT NULL UNIQUE,
			idUtil INTEGER,
			FOREIGN KEY(idUtil) REFERENCES USER(idUser));');


}

catch(PDOException $e){
	echo "oui.".$e->getMessage();
}


?>
