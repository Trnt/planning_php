<?php

  function isChampRemplis($pseudo, $pwd){
    /*
     * Je vérifie si mes champs sont remplis. S'ils ne sont pas remplis,
     * j'envoie un message disant de les remplir.
     */
    if(empty($pseudo) || empty($pwd)){
      return False;
    }

    else{
      return True;
    }
  }


   /*
    * Si isChampRemplis() est vraie, on lance cette fonction
    */
  function matchPseudoPwd($pseudo, $pwd){

    $reqPrep = "SELECT pwd FROM USER WHERE pseudo == :pseudo";

    bindParam(:pseudo, $pseudo);

    if( strcmp($pwd, $reqPrep) !== 0){
      return False;
    }

    else{
      return True;
    }
  }

  /*
   * Si matchPseudoPwd, on va faire la page d'accueil du user
   */



?>
