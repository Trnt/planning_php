<?php session_start();
 echo "<h1 class = 'bjr'> Bonjour ".$_SESSION['prenomUser']."  ".$_SESSION['nomUser']."</h1>";
 ?>
<head>
<meta charset='utf-8' />
<link href='fullcalendar-3.4.0/css/monNouveauCSS.css' rel='stylesheet'/>
<link href='fullcalendar-3.4.0/fullcalendar.min.css' rel='stylesheet' />
<link href='fullcalendar-3.4.0/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<script src='fullcalendar-3.4.0/lib/moment.min.js'></script>
<script src='fullcalendar-3.4.0/lib/jquery.min.js'></script>
<script src='fullcalendar-3.4.0/lib/jquery-ui.min.js'></script>
<script src='fullcalendar-3.4.0/fullcalendar.min.js'></script>
<script src='fullcalendar-3.4.0/locale/fr.js'></script>
<script>

	$(document).ready(function() {


		/* initialize the external events
		-----------------------------------------------------------------*/

		$('#external-events .fc-event').each(function() {

			// store data so the calendar knows to render an event upon drop
			$(this).data('event', {
				title: $.trim($(this).text()), // use the element's text as the event title
				stick: true // maintain when user navigates (see docs on the renderEvent method)
			});

			// make the event draggable using jQuery UI
			$(this).draggable({
				zIndex: 999,
				revert: true,      // will cause the event to go back to its
				revertDuration: 0  //  original position after the drag

			});

		});


		/* initialize the calendar
		-----------------------------------------------------------------*/

		var calendar = $('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'agendaDay'
			},
			editable: true,
			lang:'fr',
			defaultView: 'agendaDay',
			minTime: '08:00:00',
			maxTime:  '20:00:00',
			defaultTimedEventDuration: "01:00:00",
			slotDuration: "01:00:00",
			droppable: true, // this allows things to be dropped onto the calendar
			events: "events.php",
			
			
			drop: function(date) {
				// is the "remove after drop" checkbox checked?
				if ($('#drop-remove').is(':checked')) {
					$(this).remove();
				}
        //console.log(date);
				start = new Date( $.fullCalendar.formatDate(date, "YYYY-MM-DD HH:mm"));
				end = new Date(start);
				end.setHours(start.getHours() + 1);//Ajoute une heure (par défaut) par rapport à la date de début

				var eventObj = $(this).data('event');
				var titre = eventObj.title;


				$.ajax({
				url : "addEvent.php",
				data : '&idUser=' + <?php echo $_SESSION['idUser']; ?> + '&start=' + start + '&end=' + end + '&titre=' + titre,
				type : "POST"
					});
			},

			eventClick: function(event){
				if (confirm("Voulez vous supprimer cette tache!") == true) {
					$('#calendar').fullCalendar('removeEvents', event.id);
					alert( event.id );
				}
			},
			eventResize:function(event){
				start = $.fullCalendar.formatDate(event.start, "YYYY-MM-DD HH:mm");
				end = $.fullCalendar.formatDate(event.end, "YYYY-MM-DD HH:mm");
				$.ajax({
					url: 'update_event.php',
					 data: '&id='+event.id+'&title='+ event.title+'&start='+ start +'&end='+ end,
					 type: "POST"
				})
				
			},
			eventDrop:function(event){
				start = $.fullCalendar.formatDate(event.start, "YYYY-MM-DD HH:mm");
				end = $.fullCalendar.formatDate(event.end, "YYYY-MM-DD HH:mm");
				$.ajax({
					url: 'update_event.php',
					 data: '&id='+event.id+'&title='+ event.title+'&start='+ start +'&end='+ end,
					 type: "POST"
				})
				
			}
		}
  );


	});

</script>

</head>
<body>
	<div id='wrap'>

		<div id='external-events'>
			<h4>Activités</h4>
			<div class='fc-event'>Java</div>
			<div class='fc-event'>Repos</div>
			<div class='fc-event'>Jouer à LoL</div>
			<div class='fc-event'>Café</div>
			<div class='fc-event'>Manger</div>
			<div class='fc-event'>Dormir</div>
			<p>
				<input type='checkbox' hidden id='drop-remove' />
				<label for='drop-remove' hidden>remove after drop</label>
			</p>
		</div>

		<div id='calendar'></div>

		<div style='clear:both'></div>

		<input type="button" value="Deconnection" onclick="window.location.href='deconnection.php'"/>

	</div>
</body>
</html>
